package storage

import models.Goal
import models.Match
import models.Player
import models.Team

interface FootookStorage {
    fun addPlayer(teamId: Int, name: String): Player
    fun addTeam(teamName: String): Team
    fun addMatch(teamOneId: Int, teamTwoId: Int, location: String): Match
    fun addGoal(playerId: Int, matchId: Int): Goal
    fun getPlayer(playerId: Int): Player
    fun getTeam(teamId: Int): Team
    fun getTeams(): List<Team>
    fun getMatch(matchId: Int): Match
    fun getGoal(goalId: Int): Goal
    fun getPlayerTeam(playerId: Int): Team
    fun getPlayersOfTeam(teamId: Int): List<Player>
    fun getMatchesOfTeam(teamId: Int): List<Match>
    fun getGoalsByPlayer(playerId: Int): List<Goal>
    fun getGoalsInMatch(matchId: Int): List<Goal>
}
