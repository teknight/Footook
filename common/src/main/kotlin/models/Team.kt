package models

data class Team(val teamId: Int, val teamName: String)
