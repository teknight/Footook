package models

data class Player(val playerId: Int, val teamId: Int, val name: String)
