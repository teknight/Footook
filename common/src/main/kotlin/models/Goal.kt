package models

data class Goal(val goalId: Int, val playerId: Int, val matchId: Int)
