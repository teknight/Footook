package models

data class Match(val matchId: Int, val location: String)
