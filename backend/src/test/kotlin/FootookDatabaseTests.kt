import com.winterbe.expekt.should
import database.FootookDatabase

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import storage.FootookStorage

object FootookDatabaseTests: Spek({
    given("footook storage implementation") {
        on("insertion") {
            it("should not fail") {
                val db: FootookStorage = FootookDatabase()
                val teamOne = db.addTeam("DEN")
                val teamTwo = db.addTeam("GER")
                val player = db.addPlayer(teamOne.teamId, "Thomas")
                val match = db.addMatch(teamOne.teamId, teamTwo.teamId, "Copenhagen")
                db.addGoal(player.playerId, match.matchId)
            }
            it("should be possible to retrieve the element again") {
                val db: FootookStorage = FootookDatabase()
                val teamOneName = "SWE"
                val teamTwoName = "ENG"
                val teamOne = db.addTeam(teamOneName)
                val teamTwo = db.addTeam(teamTwoName)

                val playerName = "Christian"
                val player = db.addPlayer(teamOne.teamId, playerName)

                val location = "London"
                val match = db.addMatch(teamOne.teamId, teamTwo.teamId, location)
                val goal = db.addGoal(player.playerId, match.matchId)

                val retrievedTeamOne = db.getTeam(teamOne.teamId)
                val retrievedTeamTwo = db.getTeam(teamTwo.teamId)

                val retrievedPlayer = db.getPlayer(player.playerId)

                val retrievedMatch = db.getMatch(match.matchId)
                val retrievedGoal = db.getGoal(goal.goalId)

                retrievedTeamOne.teamName.should.equal(teamOneName)
                retrievedTeamTwo.teamName.should.equal(teamTwoName)

                retrievedPlayer.name.should.equal(playerName)
                retrievedPlayer.teamId.should.equal(teamOne.teamId)

                retrievedMatch.location.should.equal(location)

                retrievedGoal.matchId.should.equal(match.matchId)
                retrievedGoal.playerId.should.equal(player.playerId)
            }
        }
        on("team retrieval") {
            it("should return the correct team") {
                val db: FootookStorage = FootookDatabase()
                val teamOne = db.addTeam("team1")
                val teamTwo = db.addTeam("team2")
                val teamThree = db.addTeam("team3")

                db.addPlayer(teamOne.teamId, "player1")
                val player2 = db.addPlayer(teamTwo.teamId, "player2")
                db.addPlayer(teamThree.teamId, "player3")
                db.getPlayerTeam(player2.playerId).teamId.should.equal(teamTwo.teamId)
            }
        }
        on("get players of team") {
            it("should return all players of team") {
                val db: FootookStorage = FootookDatabase()
                val teamOne = db.addTeam("team1")
                val teamTwo = db.addTeam("team2")
                val teamThree = db.addTeam("team3")

                db.addPlayer(teamOne.teamId, "player1")
                val playerTwo = db.addPlayer(teamTwo.teamId, "player2")
                val playerThree = db.addPlayer(teamTwo.teamId, "player3")
                db.addPlayer(teamThree.teamId, "player4")
                val playerFive = db.addPlayer(teamTwo.teamId, "player5")
                db.getPlayersOfTeam(teamTwo.teamId).should.equal(listOf(playerTwo, playerThree, playerFive))
            }
        }
        on("get matches of team") {
            it("should return the correct matches") {
                val db: FootookStorage = FootookDatabase()
                val teamOne = db.addTeam("team1")
                val teamTwo = db.addTeam("team2")
                val teamThree = db.addTeam("team3")

                val matchOne = db.addMatch(teamOne.teamId, teamTwo.teamId, "Location1")
                db.addMatch(teamOne.teamId, teamThree.teamId, "Location2")
                val matchThree = db.addMatch(teamTwo.teamId, teamThree.teamId, "Location1")

                db.getMatchesOfTeam(teamTwo.teamId).should.equal(listOf(matchOne, matchThree))
            }
        }
        on("get goals by player") {
            it("should return the correct goals") {
                val db: FootookStorage = FootookDatabase()
                val teamOne = db.addTeam("team1")
                val teamTwo = db.addTeam("team2")

                val player = db.addPlayer(teamOne.teamId, "player")

                val matchOne = db.addMatch(teamOne.teamId, teamTwo.teamId, "Location1")
                val matchTwo = db.addMatch(teamOne.teamId, teamTwo.teamId, "Location2")

                val goalOne = db.addGoal(player.playerId, matchOne.matchId)
                val goalTwo = db.addGoal(player.playerId, matchOne.matchId)
                val goalThree = db.addGoal(player.playerId, matchTwo.matchId)

                db.getGoalsByPlayer(player.playerId).should.equal(listOf(goalOne, goalTwo, goalThree))
            }
        }

        on("get goals in match player") {
            it("should return the correct goals") {
                val db: FootookStorage = FootookDatabase()
                val teamOne = db.addTeam("team1")
                val teamTwo = db.addTeam("team2")

                val player = db.addPlayer(teamOne.teamId, "player")

                val matchOne = db.addMatch(teamOne.teamId, teamTwo.teamId, "Location1")
                val matchTwo = db.addMatch(teamOne.teamId, teamTwo.teamId, "Location2")

                val goalOne = db.addGoal(player.playerId, matchOne.matchId)
                val goalTwo = db.addGoal(player.playerId, matchOne.matchId)
                db.addGoal(player.playerId, matchTwo.matchId)

                db.getGoalsInMatch(matchOne.matchId).should.equal(listOf(goalOne, goalTwo))
            }
        }
    }
})
