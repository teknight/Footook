package api

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.ktor.pipeline.PipelineContext
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.ktor.request.receiveText
import io.ktor.response.header
import storage.FootookStorage

class RestServer(storage: FootookStorage, port: Int): ApiServer {
    private val apiVersion = "v1"
    private var mapper = ObjectMapper().registerKotlinModule()
    private val server = embeddedServer(Netty, port) {
        install(DefaultHeaders)
        install(CallLogging)
        routing {
            post("/$apiVersion/player") {
                val post = receive<PostPlayer>()
                val id = storage.addPlayer(post.teamId, post.name).playerId
                respond(mapOf("id" to id))
            }
            post("/$apiVersion/team") {
                val post = receive<PostTeam>()
                val id = storage.addTeam(post.name).teamId
                respond(mapOf("id" to id))
            }
            post("/$apiVersion/goal") {
                val post = receive<PostGoal>()
                val id = storage.addGoal(post.playerId, post.matchId).goalId
                respond(mapOf("id" to id))
            }
            post("/$apiVersion/match") {
                val post = receive<PostMatch>()
                val id = storage.addMatch(post.teamOneId, post.teamTwoId, post.location).matchId
                respond(mapOf("id" to id))
            }
            get("/$apiVersion/player/{id}") {
                respond(storage.getPlayer(param("id").toInt()))
            }
            get("/$apiVersion/team/{id}") {
                respond(storage.getTeam(param("id").toInt()))
            }
            get("/$apiVersion/goal/{id}") {
                respond(storage.getGoal(param("id").toInt()))
            }
            get("/$apiVersion/match/{id}") {
                respond(storage.getMatch(param("id").toInt()))
            }
            get("/$apiVersion/teams") {
                respond(storage.getTeams())
            }
        }
    }

    private suspend inline fun<reified T> PipelineContext<*, ApplicationCall>.receive() =
            mapper.readValue(call.receiveText(), T::class.java)
    private suspend fun PipelineContext<*, ApplicationCall>.respond(obj: Any) = call.run {
        response.headers.append("Access-Control-Allow-Origin", "*")
        respond(mapper.writeValueAsString(obj))
    }
    private fun PipelineContext<*, ApplicationCall>.param(name: String) = call.parameters[name]!!

    override fun start() {
        server.start(wait = true)
    }

    data class PostPlayer(val teamId: Int, val name: String)
    data class PostTeam(val name: String)
    data class PostGoal(val playerId: Int, val matchId: Int)
    data class PostMatch(val teamOneId: Int, val teamTwoId: Int, val location: String)
}
