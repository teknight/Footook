import api.RestServer
import database.FootookDatabase

fun main(args: Array<String>) {
    val footookStorage = FootookDatabase()
    val team = footookStorage.addTeam("Denmark")
    footookStorage.addPlayer(team.teamId, "Thomas")
    val restServer = RestServer(footookStorage, 8080)
    restServer.start()
}
