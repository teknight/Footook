package database

import database.dao.*
import exceptions.GoalNotFoundException
import exceptions.MatchNotFoundException
import exceptions.PlayerNotFoundException
import exceptions.TeamNotFoundException
import models.Goal
import models.Match
import models.Player
import models.Team
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.transactions.transaction
import storage.FootookStorage

class FootookDatabase(private val db: Database = Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver"))
    : FootookStorage {

    init {
        transaction(db) {
            create(Players, Matches, Goals, Teams, TeamMatches)
        }
    }

    private inline fun<reified T, U> transactionWithContext(crossinline f: T.() -> U): U {
        return transaction(db) {
            T::class.objectInstance?.f() ?: throw Exception("Invalid context!")
        }
    }

    private inline fun<reified T: IntIdTable, U> selectFromId(dbId: Int, crossinline modifier: ResultRow.() -> U) =
            transactionWithContext<T, U?> {
                select { id eq dbId }.firstOrNull()?.run(modifier)
            }

    private val ResultRow.asPlayer get() = Player(this[Players.id].value, this[Players.teamId].value, this[Players.name])
    private val ResultRow.asTeam get() = Team(this[Teams.id].value, this[Teams.teamName])
    private val ResultRow.asMatch get() = Match(this[Matches.id].value, this[Matches.location])
    private val ResultRow.asGoal get() = Goal(this[Goals.id].value, this[Goals.playerId].value, this[Goals.matchId].value)

    override fun addPlayer(teamId: Int, name: String) = transaction(db) {
        val id = Players.insertAndGetId {
            it[Players.teamId] = EntityID(teamId, Players)
            it[Players.name] = name
        }
        Player(id.value, teamId, name)
    }

    override fun addTeam(teamName: String) = transaction(db) {
        val id = Teams.insertAndGetId {
            it[Teams.teamName] = teamName
        }
        Team(id.value, teamName)
    }

    override fun addMatch(teamOneId: Int, teamTwoId: Int, location: String) = transaction(db) {
        val id = Matches.insertAndGetId {
            it[Matches.location] = location
        }
        TeamMatches.insert {
            it[TeamMatches.matchId] = id
            it[TeamMatches.teamId] = EntityID(teamOneId, TeamMatches)
        }
        TeamMatches.insert {
            it[TeamMatches.matchId] = id
            it[TeamMatches.teamId] = EntityID(teamTwoId, TeamMatches)
        }
        Match(id.value, location)
    }

    override fun addGoal(playerId: Int, matchId: Int) = transaction(db) {
        val id = Goals.insertAndGetId {
            it[Goals.playerId] = EntityID(playerId, Goals)
            it[Goals.matchId] = EntityID(matchId, Goals)
        }
        Goal(id.value, playerId, matchId)
    }

    override fun getPlayer(playerId: Int) =
            selectFromId<Players, Player>(playerId) { asPlayer } ?: throw PlayerNotFoundException()

    override fun getTeam(teamId: Int) =
            selectFromId<Teams, Team>(teamId) { asTeam } ?: throw TeamNotFoundException()

    override fun getTeams() = transactionWithContext<Teams, List<Team>> { selectAll().map { it.asTeam } }

    override fun getMatch(matchId: Int) =
            selectFromId<Matches, Match>(matchId) { asMatch } ?: throw MatchNotFoundException()

    override fun getGoal(goalId: Int) =
            selectFromId<Goals, Goal>(goalId) { asGoal } ?: throw GoalNotFoundException()

    override fun getPlayerTeam(playerId: Int) = transactionWithContext<Teams, Team> {
        innerJoin(Players).slice(columns).select { Players.id eq playerId }.firstOrNull()?.asTeam
                ?: throw TeamNotFoundException()
    }

    override fun getPlayersOfTeam(teamId: Int) = transactionWithContext<Teams, List<Player>> {
        innerJoin(Players).slice(Players.columns).select { Players.teamId eq teamId }.map { it.asPlayer }
    }

    override fun getMatchesOfTeam(teamId: Int) = transactionWithContext<Teams, List<Match>> {
        innerJoin(TeamMatches)
                .select { TeamMatches.teamId eq teamId }
                .map { it[TeamMatches.matchId] }
                // TODO: Make below a single query instead of O(N).
                .map { getMatch(it.value) }
    }

    override fun getGoalsByPlayer(playerId: Int) = transactionWithContext<Players, List<Goal>> {
        innerJoin(Goals).slice(Goals.columns).select { Goals.playerId eq playerId }.map { it.asGoal }
    }

    override fun getGoalsInMatch(matchId: Int) = transactionWithContext<Goals, List<Goal>> {
        innerJoin(Matches).slice(Goals.columns).select { Goals.matchId eq matchId }.map { it.asGoal }
    }
}
