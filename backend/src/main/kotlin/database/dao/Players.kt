package database.dao

import org.jetbrains.exposed.dao.IntIdTable

object Players : IntIdTable() {
    val teamId = reference("team_id", Teams)
    val name = varchar("player_name", 20)
}
