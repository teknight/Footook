package database.dao

import org.jetbrains.exposed.dao.IntIdTable

object TeamMatches: IntIdTable() {
    val teamId = reference("team_id", Teams)
    val matchId = reference("match_id", Matches)
}
