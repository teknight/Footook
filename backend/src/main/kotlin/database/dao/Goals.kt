package database.dao

import org.jetbrains.exposed.dao.IntIdTable

object Goals: IntIdTable() {
    val playerId = reference("player_id", Players)
    val matchId = reference("match_id", Matches)
}
