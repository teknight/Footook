package database.dao

import org.jetbrains.exposed.dao.IntIdTable

object Matches : IntIdTable() {
    val location = varchar("location", 20)
}
