package database.dao

import org.jetbrains.exposed.dao.IntIdTable

object Teams: IntIdTable() {
    val teamName = varchar("team_name", 20)
}
