import jsextension.launch
import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.div
import kotlinx.html.js.onClickFunction
import kotlin.browser.document
import kotlin.browser.window

fun<T> buildPage(f: suspend () -> T, content: DIV.(value: T) -> Unit) {
    launch {
        val value = f()
        document.getElementById("body")!!.outerHTML = document.create.div {
                    id = "body"
                    this.content(value)
        }.outerHTML
    }
}

fun index() {
    buildPage({ RestClient.getTeams() }) {
        h1 { +"Teams:" }
        ul {
            it.forEach {
                li {
                    +"${it.teamName}"
                    onClickFunction = { event ->
                        showTeam(it.teamId)
                    }
                }
            }
        }
    }
}

fun showTeam(teamId: Int) {
    buildPage({ RestClient.getTeam(teamId) }) {
        h1 { +"$it" }
    }
}
