import jsextension.getAndParseResult
import models.Goal
import models.Match
import models.Player
import models.Team

object RestClient {
    private const val apiPath = "http://localhost:8080/v1"
    suspend fun getPlayer(playerId: Int) = getAndParseResult("$apiPath/player/$playerId", null, ::parsePlayer)
    suspend fun getTeam(teamId: Int) = getAndParseResult("$apiPath/team/$teamId", null, ::parseTeam)
    suspend fun getTeams() = getAndParseResult("$apiPath/teams", null) { it.iterator().asSequence().map(::parseTeam) }
    private fun parsePlayer(json: dynamic) = Player(json.playerId as Int, json.teamId as Int, json.name as String)
    private fun parseTeam(json: dynamic) = Team(json.teamId as Int, json.teamName as String)
}
